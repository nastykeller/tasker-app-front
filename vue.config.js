const SitemapPlugin = require('sitemap-webpack-plugin').default
const paths = [
  {
    path: '/',
    lastmod: new Date().toISOString().slice(0,10),
    priority: '0.8',
    changefreq: 'daily'
  }
]

module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    host: 'localhost'
  },
  pwa: {
    workboxOptions: {
      skipWaiting: true
    }
  },
  configureWebpack: {
    plugins: [
      new SitemapPlugin('https://listick.io', paths, {
        filename: 'sitemap.xml',
        lastmod: true,
        changefreq: 'daily',
        priority: '0.8',
        skipgzip: true
      })
    ]
  }
}
