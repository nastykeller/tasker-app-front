import Repository from './Repository'
import store from '../store'

const searchResource = '/search'
const subscribeResource = '/subscribe'
const feedbackResource = '/feedback'
const mediaResource = '/utils/media'
const attachmentResource = '/attachments'

export default {
  search (payload) {
    return Repository.post(`${searchResource}`, payload)
  },
  subscribe (payload) {
    return Repository.post(`${subscribeResource}`, payload)
  },
  sendFeedback (payload) {
    return Repository.post(`${feedbackResource}`, payload)
  },
  getMedia (payload) {
    return Repository.post(`${mediaResource}`, payload)
  },
  getFile (payload) {
    return Repository.get(`${payload}`, {
      responseType: 'blob'
    })
  },
  uploadFile (payload) {
    return Repository.post(`${attachmentResource}`, payload, {
      onUploadProgress (progressEvent) {
        let percentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ))
        store.dispatch('setUploadPercentage', percentage)
      },
      timeout: 900000
    })
  },
  deleteFile (id) {
    return Repository.delete(`${attachmentResource}/${id}`)
  }
}