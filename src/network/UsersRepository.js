import Repository from './Repository'

const resource = '/users'

export default {
  register (payload) {
    return Repository.post(`${resource}/register`, payload)
  },
  authenticate (payload) {
    return Repository.post(`${resource}/token`, payload)
  },
  googleAuthenticate (payload) {
    return Repository.post(`${resource}/token/google`, payload)
  },
  googleAuthorizeCalendarApi (payload) {
    return Repository.post(`${resource}/calendars/permission`, payload)
  },
  googleCalendarLink (payload) {
    return Repository.post(`${resource}/calendars`, payload)
  },
  googleCalendarUnlink (id) {
    return Repository.delete(`${resource}/calendars/${id}`)
  },
  isGoogleCalendarConnected () {
    return Repository.get(`${resource}/calendars`)
  },
  googleCalendarStartSync () {
    return Repository.get(`${resource}/calendars/sync`)
  },
  appleAuthenticate (payload) {
    return Repository.post(`${resource}/token/apple`, payload)
  },
  refreshToken (payload) {
    return Repository.post(`${resource}/token/refresh`, payload)
  },
  getUser () {
    return Repository.get(`${resource}`)
  },
  update (payload) {
    return Repository.put(`${resource}`, payload)
  },
  delete (payload) {
    return Repository.delete(`${resource}`, payload)
  },
  changePassword (payload) {
    return Repository.put(`${resource}/password`, payload)
  }
}
