import axios from 'axios'
import store from '../store'

const baseDomain = 
  process.env.NODE_ENV === 'development'
  ? 'https://ukprogress30.ru:447'
  : process.env.VUE_APP_BASE_URL

const baseURL = `${baseDomain}`
const timeout = 600000

const instance = axios.create({
  baseURL,
  timeout
})

// Request interceptor region
instance.interceptors.request.use(
  config => {
    const token = store.getters.getTokenAccess
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// End region


// Response interceptor region
let isAlreadyFetchingAccessToken = false
let subscribers = []

function onAccessTokenFetched (access_token) {
  subscribers = subscribers.filter(callback => callback(access_token))
}

function addSubscriber (callback) {
  subscribers.push(callback)
}

instance.interceptors.response.use(response => {
  return response
}, error => {
  if (error.response) {
    const { config, response: { status } } = error
    const originalRequest = config

    // if token expired
    if (status === 401 && !!error.response.data.code && !!error.response.data.messages) {
      if (!isAlreadyFetchingAccessToken) {
        isAlreadyFetchingAccessToken = true

        // get new token  
        store.dispatch('refreshToken').then((access_token) => {
          isAlreadyFetchingAccessToken = false
          onAccessTokenFetched(access_token)
        })
      }

      const retryOriginalRequest = new Promise((resolve) => {
        addSubscriber(response => {
          originalRequest.headers.Authorization = `Bearer ${response.data.access}`
          resolve(axios(originalRequest))
        })
      })
      return retryOriginalRequest
    }

    // if wrong cred, pass handle error to vuex action login
    if (status === 401 && error.response.data.detail) return

    if (status === 500) {
      store.dispatch('snackbar/show', {
        msg: 'Ошибка сервера',
        type: 'error'
      })
    }
  } else {
    store.dispatch('snackbar/show', {
      msg: 'Проблемы с подключением к сети…',
      type: 'error'
    })
  }
  return Promise.reject(error)
})
// End region

export default instance
