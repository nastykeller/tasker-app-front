import Repository from './Repository'

const resource = '/tasks'

export default {
  getAll () {
    return Repository.get(`${resource}`)
  },
  getNewest () {
    return Repository.get(`${resource}/newest`)
  },
  create (payload) {
    return Repository.post(`${resource}`, payload)
  },
  getDetail (id) {
    return Repository.get(`${resource}/${id}`)
  },
  update (id, payload) {
    return Repository.put(`${resource}/${id}`, payload)
  },
  delete (id) {
    return Repository.delete(`${resource}/${id}`)
  }
}