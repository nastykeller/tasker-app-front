import UsersRepository from './UsersRepository'
import TasksRepository from './TasksRepository'
import EventsRepository from './EventsRepository'
import UtilsRepository from './UtilsRepository'
import ProjectsRepository from './ProjectsRepository'

const repositories = {
  users: UsersRepository,
  tasks: TasksRepository,
  events: EventsRepository,
  utils: UtilsRepository,
  projects: ProjectsRepository
}

export const RepositoryFactory = {
  get: name => repositories[name]
}