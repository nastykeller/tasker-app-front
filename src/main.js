import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import Vuelidate from 'vuelidate'
import InfiniteScroll from 'v-infinite-scroll'
import 'v-infinite-scroll/dist/v-infinite-scroll.css'
import GAuth from 'vue-google-oauth2'
import VueGAPI from 'vue-gapi'
import VueInputAutowidth from 'vue-input-autowidth'
import './registerServiceWorker'
import VueObserveVisibility from 'vue-observe-visibility'
import i18n from '@/locale/i18n'

Vue.use(VueObserveVisibility)
Vue.use(VueInputAutowidth)

/** ⚡ GAuth region */
const gauthOption = {
  clientId: '397207331958-voe7upnai1mm2u6425qqodcti21utl5c.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}

Vue.use(GAuth, gauthOption)
/** ⚡ End GAuth region */

/** VueGAPI calendar region */
const gapiConfig = {
  discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest']
}
Vue.use(VueGAPI, gapiConfig)
/** End VueGAPI calendar region */

Vue.use(Vuelidate)
Vue.config.productionTip = false

const VueScrollTo = require('vue-scrollto')
Vue.use(VueScrollTo)

Vue.use(InfiniteScroll)

export const EventBus = new Vue()

/**
 * NOTE: change render method cause checking route.meta meta lead to flickering
 * (show Navigation component appears while route.meta is checking in App.vue) 
 * during app loading (landing page)
 **/
const vm = new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
})//.$mount('#app')

store.$app = vm

window.onload = function () {
  vm.$mount('#app')
}
