import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home'),
    meta: { 
      Independent: true,
      requiresAuth: false,
      Homepage: true
    }
  },
  {
    path: '/login',
    name: 'Auth',
    component: () => import('../views/Login'),
    meta: { 
      Independent: true,
      name: 'Auth',
      hideFromAuthenticated: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register'),
    meta: {
      Independent: true,
      name: 'Register',
      hideFromAuthenticated: true
    }
  },
  {
    path: '/tasks',
    name: 'Tasks',
    props: true,
    component: () => import('../views/Pages/Tasks.vue'),
    meta: { 
      Independent: false,
      requiresAuth: true,
      name: 'Tasks'
    }
  },
  {
    path: '/events',
    name: 'Events',
    props: true,
    component: () => import('../views/Pages/Events.vue'),
    meta: { 
      Independent: false,
      requiresAuth: true,
      name: 'Events'
    }
  },
  {
    path: '/project/:id',
    name: 'Project',
    props: true,
    component: () => import('../views/Pages/Project.vue'),
    meta: { 
      Independent: false,
      requiresAuth: true,
      name: 'Project'
    },
    beforeEnter (to, from, next) {
      if (!store.getters.validProjId(to.params.id)) {
        next('/404')
      } else {
        next()
      }
    }
  },
  {
    path: '/account',
    name: 'Account',
    component: () => import('../views/Settings/Account.vue'),
    meta: { 
      Independent: false,
      requiresAuth: true,
      name: 'Account'
    }
  },
  {
    path: '/feedback',
    name: 'Feedback',
    component: () => import('../views/Feedback.vue'),
    meta: {
      Independent: false,
      requiresAuth: false,
      name: 'Feedback'
    }
  },
  {
    path: '/404',
    name: 'PageNotFound',
    component: () => import('../views/PageNotFound.vue'),
    meta: {
      name: 'PageNotFound',
      Independent: store.getters.isAuth ? false : true,
      requiresAuth: false
    }
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

/**
 * Check auth logic
 */
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth) && !store.getters.isAuth) {
    next('/login') 
  } else {
    if (to.meta.hideFromAuthenticated && store.getters.isAuth) {
      router.push('/tasks')
    } else {
      next()
    }
  }
})

export default router
