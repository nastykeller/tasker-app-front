import { RepositoryFactory } from '../../network/RepositoryFactory'
const TasksRepository = RepositoryFactory.get('tasks')

const getDefaultTasksState = () => {
  return {
    tasks: [],
    currentTask: null
  }
}

const state = getDefaultTasksState()

const mutations = {
  setTasks (state, payload) {
    state.tasks = payload.data
  },
  setCurrentTask (state, payload) {
    const task = state.tasks.find(task => task.id === payload.id)
    state.currentTask = task
  },
  deleteCurrentTask (state) {
    state.currentTask = null
  },
  resetTasksState (state) {
    Object.assign(state, getDefaultTasksState())
  },
  updateCurrentTask (state, payload) {
    state.currentTask = payload.data
  }
}

const actions = {
  async getTasks ({commit}) {
    const data = await TasksRepository.getAll()
    commit('setTasks', data)
  },
  addTask ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
        TasksRepository.create({
        name: payload.name,
        description: '',
        important: false,
        notification: payload.notification ? payload.notification : null,
        project: payload.project ? payload.project : null,
        done: false
      }).then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.add'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.addError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  setCurrentTask ({commit}, payload) {
    commit('setCurrentTask', payload)
  },
  deleteCurrentTask ({commit}) {
    commit('deleteCurrentTask')
  },
  async deleteTask ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      TasksRepository.delete(payload).then((response) => {
        dispatch('getTasks')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.delete'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.deleteError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  updateTask ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      let value = payload['type'] === 'time' ? payload.value : { [payload['type']]: payload.value }
      let params = {
        msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.update'),
        type: 'info'
      }
      switch (payload['type']) {
        case 'done':
          params.msg = this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.done')
          break
        case 'time':
          params = {
            msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.time.becomeEvent'),
            type: 'info',
            action: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.go'),
            route: 'Events'
          }
          break
        case 'project':
          // if proj was set
          if (payload.value != null) {
            params = {
              msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.project.add'),
              type: 'info',
              action: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.goToProj'),
              path: `/project/${payload.value}`
            }
          // if proj was drop
          } else {
            params = {
              msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.project.drop'),
              type: 'info',
              action: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.goToTasks'),
              route: 'Tasks'
            }
          }
          break
        default:
          params.msg = this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.update')
          break
      }
      TasksRepository.update(payload.id, value).then((response) => {
        // if done: false, hide snackbar
        if (payload['type'] === 'done' && !payload.value) return
        dispatch('snackbar/show', params, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Tasks.updateError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  resetTasksState ({ commit }) {
    commit('resetTasksState')
  },
  async updateCurrentTask ({commit}, payload) {
    const data = await TasksRepository.getDetail(payload.id)
    commit('updateCurrentTask', data)
  },
  async getNewestTask () {
    return new Promise((resolve, reject) => {
      TasksRepository.getNewest().then((response) => {
        let data = response.data
        resolve(data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}

const getters = {
  tasks: state => {
    return state.tasks
  },
  currentTask: state => {
    return state.currentTask
  },
  tasksCount: state => {
    return state.tasks.length
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
