const getDefaultSnackbarState = () => {
  return {
    isShowing: false,
    params: {
      msg: null,
      type: null,
      action: null
    }
  }
}

const state = getDefaultSnackbarState()

const mutations = {
  show (state, payload) {
    state.isShowing = false
    setTimeout(() => {
      state.params = payload
      state.isShowing = true
    }, 250)
  },
  hide (state) {
    state.isShowing = false
    state.params = {
      msg: null,
      type: null,
      action: null
    }
  }
}
const actions = {
  show ({commit}, payload) {
    commit('show', payload)
  },
  hide ({commit}) {
    commit('hide')
  }
}
const getters = {
  isShowing: state => {
    return state.isShowing
  },
  params: state => {
    return state.params
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}