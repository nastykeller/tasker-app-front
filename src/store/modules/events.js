import { RepositoryFactory } from '../../network/RepositoryFactory'
const EventsRepository = RepositoryFactory.get('events')
import router from '@/router'
import moment from 'moment'
import 'moment/locale/ru'

const getDefaultEventsState = () => {
  return {
    events: [],
    currentEvent: null,
    calendarDate: moment().format('YYYY-MM-DD'),
    feedDays: {
      start: null,
      end: null
    },
    feedDaysForward: {
      start: null,
      end: null
    },
    feedDaysBackward: {
      start: null,
      end: null
    },
    feedStep: 7,
    feedInitialStep: 15
  }
}

const state = getDefaultEventsState()

const mutations = {
  setEvents (state, payload) {
    state.events = payload.data
  },
  appendEvents (state, payload) {
    state.events.push(...payload.data)
  },
  prependEvents (state, payload) {
    state.events.unshift(...payload.data)
  },
  setCurrentEvent (state, payload) {
    let event
    state.events.forEach(element => {
      element.events.forEach(item => {
        if (item.id === payload.id) {
          event = item
        }
      })
    })
    state.currentEvent = event
  },
  deleteCurrentEvent (state) {
    state.currentEvent = null
  },
  updateCurrentEvent (state, payload) {
    state.currentEvent = payload.data
  },
  setCalendarDate (state, payload) {
    state.calendarDate = payload
  },
  setFeedDays (state) {
    state.feedDays = {
      start: moment(state.calendarDate).subtract(state.feedInitialStep, 'days').format(),
      end: moment(state.calendarDate).add(state.feedInitialStep, 'days').format()
    }
    state.feedDaysForward = {
      start: moment(state.feedDays.end).add(1, 'days').format(),
      end: moment(state.feedDays.end).add(state.feedStep, 'days').format()
    }
    state.feedDaysBackward = {
      end: moment(state.feedDays.start).subtract(1, 'days').format(),
      start: moment(state.feedDays.start).subtract(state.feedStep, 'days').format()
    }
  },
  forvardFeed (state) {
    state.feedDaysForward = {
      start: moment(state.feedDaysForward.end).add(1, 'days').format(),
      end: moment(state.feedDaysForward.end).add(state.feedStep, 'days').format()
    }
  },
  backwardFeed (state) {
    state.feedDaysBackward = {
      end: moment(state.feedDaysBackward.start).subtract(1, 'days').format(),
      start: moment(state.feedDaysBackward.start).subtract(state.feedStep, 'days').format()
    }
  },
  updateFeedDaysForvard (state) {
    state.feedDays = {
      start: state.feedDays.start,
      end: state.feedDaysForward.end
    }
  },
  updateFeedDaysBackward (state) {
    state.feedDays = {
      start: state.feedDaysBackward.start,
      end: state.feedDays.end
    }
  },
  resetCalendarToToday (state) {
    state.calendarDate = moment().format('YYYY-MM-DD')
  },
  resetEventsState (state) {
    Object.assign(state, getDefaultEventsState())
  },
}

const actions = {
  async getEvents ({commit}) {
    const data = await EventsRepository.getAll()
    commit('setEvents', data)
  },
  async getGrouppedEvents ({commit, state}) {
    const data = await EventsRepository.getGroupped(state.feedDays)
    commit('setEvents', data)
  },
  async appendGrouppedEvents ({commit, state}) {
    const data = await EventsRepository.getGroupped(state.feedDaysForward)
    commit('appendEvents', data)
    commit('updateFeedDaysForvard')
    commit('forvardFeed', data)
  },
  async prependGrouppedEvents ({commit, state}) {
    const data = await EventsRepository.getGroupped(state.feedDaysBackward)
    commit('prependEvents', data)
    commit('updateFeedDaysBackward')
    commit('backwardFeed', data)
  },
  setCurrentEvent ({commit}, payload) {
    commit('setCurrentEvent', payload)
  },
  deleteCurrentEvent ({commit}) {
    commit('deleteCurrentEvent')
  },
  async updateEvent ({dispatch, getters}, payload) {
    return new Promise((resolve, reject) => {
      let value = payload['type'] === 'time' ? value = payload.value : value = {[payload['type']]: payload.value}
      let params = {
        msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.update'),
        type: 'info'
      }
      switch (payload['type']) {
        case 'done':
          params.msg = this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.done')
          break
        case 'time':
        case 'full_day':
          if (!payload.value.start && !payload.value.end) {
            params = {
              // set msg property
              msg: (router.currentRoute.name === 'Project') 
                      ? (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.time.drop')) 
                      : ((router.currentRoute.name === 'Events' && getters.currentEvent.project) 
                          ? (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.time.drop')) 
                          : (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.time.becomeTask'))),
              type: 'info',
              // set action text depends on msg property
              action: (router.currentRoute.name === 'Project') 
                      ? (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.close')) 
                      : ((router.currentRoute.name === 'Events' && getters.currentEvent.project) 
                          ? (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.goToProj')) 
                          : (this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.go'))),
              // set route action depends on action property
              ...(router.currentRoute.name === 'Project' ? { route: null } : { route: (router.currentRoute.name === 'Events' && getters.currentEvent.project) ? null : 'Tasks' }),
              // if Event page + project = redirect to Proj with id page - set path
              path: getters.currentEvent.project ? `/project/${getters.currentEvent.project.id}` : null
            }
          }
          break
        case 'project':
          // if proj was set
          if (payload.value != null) {
            params = {
              msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.project.add'),
              type: 'info',
              action: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.goToProj'),
              path: `/project/${payload.value}`
            }
          // if proj was drop
          // if proj was drop on Events page, switch action to Close
          } else {
            params = {
              msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.project.drop'),
              type: 'info',
              action: router.currentRoute.name === 'Events' ? this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.close') : this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Action.goToEvents'),
              route: router.currentRoute.name === 'Events' ? null : 'Events'
            }
          }
          break
        default:
          params.msg = this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.update')
          break
      }
      EventsRepository.update(payload.id, value).then((result) => {
        // if done: false, hide snackbar
        if (payload['type'] === 'done' && !payload.value) return
        dispatch('snackbar/show', params, { root: true }) 
        resolve(result)
      }).catch((err) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.updateError'),
          type: 'error'
        }, { root: true }) 
        reject(err)
      })
    })
  },
  async updateCurrentEvent ({commit}, payload) {
    return new Promise((resolve, reject) => {
      EventsRepository.getDetail(payload.id).then((result) => {
        commit('updateCurrentEvent', result)
        resolve(result)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async deleteEvent ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      EventsRepository.delete(payload).then((response) => {
        dispatch('getGrouppedEvents')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.delete'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.deleteError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  setCalendarDate ({commit}, payload) {
    commit('setCalendarDate', payload)
  },
  async addEvent ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      EventsRepository.create({
        name: payload.name,
        description: '',
        start: payload.start,
        end: payload.end,
        full_day: payload.full_day,
        notification: payload.notification ? payload.notification : null,
        project: payload.project ? payload.project : null,
      }).then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.add'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Events.addError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  async getNewestEvent () {
    return new Promise((resolve, reject) => {
      EventsRepository.getNewest().then((response) => {
        let data = response.data
        resolve(data)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  setFeedDays ({commit}) {
    commit('setFeedDays')
  },
  forvardFeed ({commit}) {
    commit('forvardFeed')
  },
  backwardFeed ({commit}) {
    commit('backwardFeed')
  },
  resetEventsState ({ commit }) {
    commit('resetEventsState')
  },
  async shiftFeedToDate ({commit, dispatch}, payload) {
    commit('setCalendarDate', payload)
    commit('setFeedDays')
    await dispatch('getGrouppedEvents')
  },
  resetCalendarToToday ({ commit }) {
    commit('resetCalendarToToday')
  },
}

const getters = {
  events: state => {
    return state.events
  },
  currentEvent: state => {
    return state.currentEvent
  },
  flattenEvents: state => {
    return [].concat(...state.events.map(o => o.events))
  },
  eventsCount: state =>  {
    let flatEvents = [].concat(...state.events.map(o => o.events))
    return flatEvents.length
  },
  calendarDate: state => {
    return state.calendarDate
  },
  feedDays: state => {
    return state.feedDays
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}