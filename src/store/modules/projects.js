import { RepositoryFactory } from '../../network/RepositoryFactory'
const ProjectsRepository = RepositoryFactory.get('projects')

const getDefaultEventsState = () => {
  return {
    projects: [],
    flattenProjects: [],
    projectItems: [],
    currentProjItem: null
  }
}

const state = getDefaultEventsState()

const mutations = {
  setProjects (state, payload) {
    state.projects = payload.data
  },
  setFlattenProjects (state, payload) {
    state.flattenProjects = payload.data
  },
  setProjectItems (state, payload) {
    state.projectItems = payload
  },
  setCurrentProjItem (state, { payload, rootGetters }) {
    // find projItem in events / tasks / projItems
    let events = rootGetters.flattenEvents
    let tasks = rootGetters.tasks
    let projectItems = rootGetters.projectItems

    const item = (events.some(item => item.id === payload.id)) 
      ? (events.find(item => item.id === payload.id)) 
      : ((projectItems.some(item => item.id === payload.id)) 
        ? (projectItems.find(item => item.id === payload.id)) 
        : (tasks.find(item => item.id === payload.id)))

    state.currentProjItem = item
  },
  deleteCurrentProjItem (state) {
    state.currentProjItem = null
  },
}

const actions = {
  async getProjects ({commit}) {
    const data = await ProjectsRepository.getAll()
    commit('setProjects', data)
  },
  async getAllFlattenProjects ({commit}) {
    const data = await ProjectsRepository.getAllFlatten()
    commit('setFlattenProjects', data)
  },
  createProject ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      ProjectsRepository.create({
        name: payload.name,
        parent: payload.parent,
        image: payload.image
      }).then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.add'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.addError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  deleteProject ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      ProjectsRepository.delete(payload).then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.delete'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.deleteError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  updateProject ({dispatch}, payload) {
    return new Promise((resolve, reject) => {
      ProjectsRepository.update(payload.id, payload).then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.update'),
          type: 'info'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Project.updateError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  getProjectData ({commit}, payload) {
    return new Promise((resolve, reject) => {
      ProjectsRepository.getDetail(payload.id)
      .then((response) => {
        let data = response.data
        let projectItems = data.task_set
        commit('setProjectItems', projectItems)
        resolve(data)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  setCurrentProjItem ({commit, rootGetters}, payload) {
    commit('setCurrentProjItem', { payload, rootGetters })
  },
  deleteCurrentProjItem ({commit}) {
    commit('deleteCurrentProjItem')
  },
}

const getters = {
  projects: state => {
    return state.projects
  },
  flattenProjects: state => {
    return state.flattenProjects
  },
  projectItems: state => {
    return state.projectItems
  },
  currentProjItem: state => {
    return state.currentProjItem
  },
  validProjId: (state) => id => {
    let onlyNum = /^\d+$/
    let isNum = onlyNum.test(id)

    let isValidProjId
    if (isNum) {
      isValidProjId = state.flattenProjects.find(item => item.id === Number(id)) ? true : false
    } 
    return isValidProjId
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}