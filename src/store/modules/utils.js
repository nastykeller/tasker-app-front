import router from '@/router'
import { RepositoryFactory } from '../../network/RepositoryFactory'
const UsersRepository = RepositoryFactory.get('users')
const UtilsRepository = RepositoryFactory.get('utils')

const getDefaultUtilsState = () => {
  return {
    tokenAccess: null,
    tokenRefresh: null,
    isGoogleCalendarLinked: false,
    googleCalendarLinked: null,
    locale: 'ru',
    user: null,
    status: null,
    searchQuery: null,
    searchResults: [],
    searchResultsTotal: null,
    projectBackgrounds: [],
    uploadPercentage: 0
  }
}

const state = getDefaultUtilsState()

const mutations = {
  setTokens (state, payload) {
    state.tokenAccess = payload.access
    state.tokenRefresh = payload.refresh
  },
  updateAccessToken (state, payload) {
    state.tokenAccess = payload.data.access
  },
  setGoogleCalendarStatus (state, payload) {
    state.isGoogleCalendarLinked = payload
  },
  setGoogleCalendarLinked (state, payload) {
    state.googleCalendarLinked = payload
  },
  setLocale (state, payload) {
    state.locale = payload
  },
  setUser (state, payload) {
    state.user = payload
  },
  request (state) {
    state.status = 'loading'
  },
  success (state) {
    state.status = 'success'
  },
  error (state) {
    state.status = 'error'
  },
  setSearchQuery (state, payload) {
    state.searchQuery = payload
  },
  setSearchResults (state, payload) {
    state.searchResultsTotal = payload.total
    state.searchResults = payload.data
  },
  resetSearchResults (state) {
    state.searchResultsTotal = null
    state.searchResults = []
  },
  resetSearch (state) {
    state.searchQuery = null
  },
  setProjectBackgrounds (state, payload) {
    state.projectBackgrounds = payload
  },
  setUploadPercentage (state, payload) {
    state.uploadPercentage = payload
  },
  resetUploadPercentage (state) {
    state.uploadPercentage = 0
  },
  resetUtilsState (state) {
    Object.assign(state, getDefaultUtilsState())
  }
}

const actions = {
  getUserInfo ({commit}) {
    return new Promise((resolve) => {
      commit('request')
      UsersRepository.getUser().then((response) => {
        commit('setUser', response.data)
        commit('success')
        resolve(response)
      })
    })
  },
  register ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.register({
        full_name: payload.fullName,
        email: payload.email,
        password: payload.password
      })
      .then((response) => {
        commit('setUser', response.data)
        commit('success')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Register.success'),
          type: 'success'
        }, { root: true })
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Register.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  login ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.authenticate({
        email: payload.email,
        password: payload.password
      })
      .then((response) => {
        commit('success')
        commit('setTokens', response.data)
        resolve(response)
        commit('request')
        UsersRepository.getUser().then((response) => {
          commit('setUser', response.data)
          commit('success')
          // hide msg when re-auth when pass change
          if (router.currentRoute.name === '/account') return
          /**
           * hide msg when hidden-auth 
           * when register account
           * or change pass
           */ 
          if (payload.register || payload.changePassword) return
          dispatch('snackbar/show', {
            msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.success'),
            type: 'success'
          }, { root: true })
          resolve(response)
        })
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  logout ({commit, dispatch}) {
    return new Promise((resolve) => {
      commit('resetUtilsState')
      dispatch('snackbar/show', {
        msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Logout.message'),
        type: 'info'
      }, { root: true })
      resolve()
    })
  },
  changePassword ({commit, dispatch, getters}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.changePassword({
        password: payload.password,
        new_password: payload.newPassword
      })
      .then((response) => {
        commit('success')
        dispatch('login', {
          email: getters.getUser.email.toLowerCase(),
          password: payload.newPassword,
          changePassword: true
        })
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.PasswordChange.success'),
          type: 'info'
        }, { root: true })
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.PasswordChange.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  refreshToken ({commit, getters}) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.refreshToken({
        refresh: getters.getTokenRefresh
      })
      .then((response) => {
        commit('updateAccessToken', response)
        commit('success')
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        reject(error)
      })
    })
  },
  updateUser ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.update({
        full_name: payload.fullName,
        email: payload.email
      })
      .then((response) => {
        commit('request')
          commit('setUser', response.data)
          commit('success')
          dispatch('snackbar/show', {
            msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.UserUpdate.success'),
            type: 'info'
          }, { root: true })
          resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.UserUpdate.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  deleteUser ({commit}) {
    return new Promise((resolve) => {
      commit('request')
      UsersRepository.delete().then(() => {
        resolve()
      })
    })
  },
  search ({commit, getters}) {
    return new Promise((resolve) => {
      commit('request')
      UtilsRepository.search({ 
        search: getters.searchQuery 
      }).then((response) => {
        commit('success')
        commit('setSearchResults', response.data)
        resolve(response)
      })
    })
  },
  resetSearchResults ({commit}) {
    commit('resetSearchResults')
  },
  resetSearch ({commit}) {
    commit('resetSearch')
  },
  subscribe ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UtilsRepository.subscribe(payload)
      .then((response) => {
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Subscribe.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  googleSignIn ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.googleAuthenticate({
        id_token: payload 
      })
      .then((response) => {
        commit('success')
        commit('setTokens', response.data)
        resolve(response)
        commit('request')
        UsersRepository.getUser().then((response) => {
          commit('setUser', response.data)
          commit('success')
          dispatch('snackbar/show', {
            msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.success'),
            type: 'success'
          }, { root: true })
          resolve(response)
        })
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  googleCalendarAuthorize ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.googleAuthorizeCalendarApi({
        code: payload 
      })
      .then((response) => {
        commit('success')
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.GoogleMessages.linkCalendarError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  googleCalendarLink ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.googleCalendarLink({
        gcal_id: payload.id,
        name: payload.name 
      })
      .then((response) => {
        commit('success')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.GoogleMessages.linkCalendarSuccess'),
          type: 'success'
        }, { root: true })
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.GoogleMessages.linkCalendarError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  googleCalendarUnlink ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.googleCalendarUnlink(payload)
      .then((response) => {
        commit('success')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.GoogleMessages.unlinkCalendarSuccess'),
          type: 'info'
        }, { root: true })
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.GoogleMessages.unlinkCalendarError'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  isGoogleCalendarConnected ({commit}) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.isGoogleCalendarConnected()
      .then((response) => {
        let status
        switch (response.status) {
          case 204:
            status = false
            break
          case 200:
            status = true
            break
        }
        commit('setGoogleCalendarStatus', status)
        commit('setGoogleCalendarLinked', response.data)
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
    })
  },
  googleCalendarStartSync ({commit}) {
    return new Promise((resolve, reject) => {
      commit('request')
      UsersRepository.googleCalendarStartSync()
      .then((response) => {
        commit('success')
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  appleSignIn ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      UsersRepository.appleAuthenticate({
        code: payload 
      })
      .then((response) => {
        commit('success')
        commit('setTokens', response.data)
        resolve(response)
        commit('request')
        UsersRepository.getUser().then((response) => {
          commit('setUser', response.data)
          commit('success')
          dispatch('snackbar/show', {
            msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.success'),
            type: 'success'
          }, { root: true })
          resolve(response)
        })
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Login.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  sendFeedback ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UtilsRepository.sendFeedback(payload)
      .then((response) => {
        resolve(response)
      })
      .catch(error => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$vuetify.lang.t('$vuetify.Pages.Snackbar.Feedback.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  getProjectBackgrounds ({commit}, payload) {
    return new Promise((resolve) => {
      commit('request')
      UtilsRepository.getMedia(payload)
      .then((response) => {
        commit('setProjectBackgrounds', response.data)
        resolve(response)
      })
    })
  },
  setLocale ({commit}, payload) {
    commit('setLocale', payload)
  },
  getFile ({commit}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UtilsRepository.getFile(payload)
      .then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  uploadFile ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UtilsRepository.uploadFile(payload)
      .then((response) => {
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Files.upload.success'), 
          type: 'success'
        }, { root: true })
        commit('success')
        resolve(response)
      }).catch((error) => {
        let errorMsg
        // if empty file
        if (error.response.data.name || error.response.data.file) {
          errorMsg = this.$app.$vuetify.lang.t('$vuetify.Pages.DetailView.DetailViewCard.files.emptyFileErrorMessage')
        }
        // check status messages
        if (error.response.data.file && !!Object.getOwnPropertyDescriptor(error.response.data.file, 'status')) {
            switch (error.response.data.file.status) {
            case '001':
              errorMsg = this.$app.$vuetify.lang.t('$vuetify.Pages.DetailView.DetailViewCard.files.maxFileSizeErrorMessage')
              break
            case '002':
              errorMsg = this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Files.upload.limitError')
              break
            case '003':
              errorMsg = this.$app.$vuetify.lang.t('$vuetify.Pages.DetailView.DetailViewCard.files.maxFilesCountErrorMessage')
              break
          }
        }
        commit('error', error)
          dispatch('snackbar/show', {
            msg: errorMsg ? errorMsg : this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Files.upload.commonError'),
            type: 'error'
          }, { root: true })
          reject(error)
      })
    })
  },
  deleteFile ({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('request')
      UtilsRepository.deleteFile(payload)
      .then((response) => {
        commit('success')
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Files.delete.success'), 
          type: 'success'
        }, { root: true })
        resolve(response)
      }).catch((error) => {
        commit('error', error)
        dispatch('snackbar/show', {
          msg: this.$app.$vuetify.lang.t('$vuetify.Pages.Snackbar.Files.delete.error'),
          type: 'error'
        }, { root: true })
        reject(error)
      })
    })
  },
  setUploadPercentage ({commit}, payload) {
    commit('setUploadPercentage', payload)
  },
  resetUploadPercentage ({commit}) {
    commit('resetUploadPercentage')
  },
}

const getters = {
  isAuth: state => {
    return !!state.tokenAccess
  },
  getTokenAccess: state => {
    return state.tokenAccess
  },
  getTokenRefresh: state => {
    return state.tokenRefresh
  },
  isGoogleCalendarLinked: state => {
    return state.isGoogleCalendarLinked
  },
  googleCalendarLinked: state => {
    return state.googleCalendarLinked
  },
  locale: state => {
    return state.locale
  },
  getUser: state => {
    return state.user
  },
  searchQuery: state => {
    return state.searchQuery
  },
  searchResults: state => {
    return state.searchResults
  },
  searchResultsTotal: state => {
    return state.searchResultsTotal
  },
  projectBackgrounds: state => {
    return state.projectBackgrounds
  },
  uploadPercentage: state => {
    return state.uploadPercentage
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}