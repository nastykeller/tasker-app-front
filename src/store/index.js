import Vue from 'vue'
import Vuex from 'vuex'

import utils from './modules/utils'
import tasks from './modules/tasks'
import events from './modules/events'
import projects from './modules/projects'
import snackbar from './modules/snackbar'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {},
  mutations: {},
  actions: {},
  modules: {
    utils: utils,
    tasks: tasks,
    events: events,
    projects: projects,
    snackbar: snackbar
  }
})
