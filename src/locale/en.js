const messages = {
  $vuetify: {
    // LOGIN PAGE
    Login: {
      LoginBtn: 'Login',
      LoginForm: {
        email: {
          label: 'Email',
          requiredErrorMsg: 'Field is required',
          commonErrorMsg: 'Please, provide correct data'
        },
        password: {
          label: 'Password',
          requiredErrorMsg: 'Field is required',
          minLengthErrorMsg: 'Password must be at least 8 characters long',
          maxLengthErrorMsg: 'Password must be less than 40 characters long',
          noSpacesErrorMsg: 'Password cannot contain spaces',
        },
        divider: 'or',
        googleBtnMessage: 'Login with Google',
        appleBtnMessage: 'Login with Apple',
        passwordMessage: 'Forgot password?',
        accountMessage: 'Don’t have an account?',
      }
    },
    // REGISTER PAGE
    Register: {
      RegisterBtn: 'Sign up',
      RegisterForm: {
        email: {
          label: 'Email',
          requiredErrorMsg: 'Field is required',
          commonErrorMsg: 'Please, provide correct data'
        },
        name: {
          label: 'Name',
          requiredErrorMsg: 'Field is required'
        },
        password: {
          label: 'Password',
          requiredErrorMsg: 'Field is required',
          minLengthErrorMsg: 'Password must be at least 8 characters long',
          maxLengthErrorMsg: 'Password must be less than 40 characters long',
          noSpacesErrorMsg: 'Password cannot contain spaces',
        },
        divider: 'or',
        googleBtnMessage: 'Continue with Google',
        appleBtnMessage: 'Continue with Apple',
        accountMessage: 'Already have an account?',
      }
    },
    // ACCOUNT PAGE
    Account: {
      header: 'Settings',
      ProfileCard: {
        title: 'Profile',
        form: {
          name: {
            label: 'Name',
            requiredErrorMsg: 'Field is required',
          },
          email: {
            label: 'Email',
            requiredErrorMsg: 'Field is required',
            commonErrorMsg: 'Check up correctness of all input data'
          },
          ProfileCardBtn: 'Save'
        }
      },
      PasswordChangeCard: {
        title: 'Password',
        form: {
          oldPassword: {
            label: 'Old password',
            requiredErrorMsg: 'Field is required',
            noSpacesErrorMsg: 'Password cannot contain spaces',
          },
          newPassword: {
            label: 'New password',
            requiredErrorMsg: 'Field is required',
            isNotSameAsPasswordErrorMsg: 'Your new password must be different',
            minLengthErrorMsg: 'Password must be at least 8 characters long',
            maxLengthErrorMsg: 'Password must be less than 40 characters long',
            noSpacesErrorMsg: 'Password cannot contain spaces',
          },
          newPasswordConfirm: {
            label: 'Confirm new password',
            requiredErrorMsg: 'Field is required',
            sameAsPasswordErrorMsg: 'Passwords do not match',
            minLengthErrorMsg: 'Password must be at least 8 characters long',
            maxLengthErrorMsg: 'Password must be less than 40 characters long',
            noSpacesErrorMsg: 'Password cannot contain spaces',
          },
          PasswordChangeCardBtn: 'Change'
        },
      },
      GoogleCalendarLinkCard: {
        title: 'Google Calendar',
        caption: 'Connect a 2-way sync between your Listick events and your Google Calendar.',
        GoogleCalendarLinkCardBtn: 'Sign In with Google',
        syncStatus: {
          message: 'All events sync with calendar {gcalName}. All events from {gcalName} Calendar added to Listick Feed, and all events from Listick Feed added to {gcalName} Calendar. ',
          syncStatusBtn: 'Cancel sync'
        },
        calendarListDialog: {
          title: 'Connect to Google Calendar',
          selectCalendarText: 'Pick a calendar',
          discardBtn: 'Cancel',
          confirmBtn: 'Connect'
        }
      },
      LanguageSettingsCard: {
        title: 'Language',
        selectLanguageLabel: 'Language',
        LanguageSettingsCardBtn: 'Save', 
        languages: [
          'Русский',
          'English'
        ]
      },
      DeleteAccountCard: {
        title: 'Delete your account',
        userWarningText: 'Please note that if you delete your Listick account, access to all the tasks and events in your account will be lost.',
        DeleteAccountCardBtn: 'Delete account',
        deleteAccountDialog: {
          title: 'Delete account',
          deleteWarningText: 'Are you sure? This action is irreversible!',
          discardBtn: 'Cancel',
          confirmBtn: 'OK'
        }
      },
      StorageCard: {
        title: 'Storage',
        StorageCardBtn: 'Increase storage size',
        storageUsage: 'Used {usedStorageSize} of {fullStorageSize}'
      },
      LogoutCard: {
        title: 'Log out',
        caption: 'Log out from Listick',
        LogoutCardBtn: 'Log out'
      },
      feedback: {
        text: 'Have Question?',
        link: 'Contact us'
      },
    },
    Feedback: {
      title: 'Contact us',
      name: {
        label: 'Name'
      },
      email: {
        label: 'Email',
        commonErrorMsg: 'Check up correctness of all input data'
      },
      message: {
        label: 'Message'
      },
      FeedbackBtn: 'Save'
    },
    Home: {
      title: 'Plan newly with Listick',
      caption: `Keep all your ideas, events and tasks in Listick. We will remind you about it.
      Download our app!`,
      navbar: {
        login: 'Log in',
        register: 'Sign up'
      },
      subscribeForm: {
        name: {
          placeholder: 'Email'
        },
        btn: 'Subscribe'
      },
      startBlock: {
        title: 'Start simple',
        caption: `Listick can be used however you like. 
        Use Tasks if something come to your mind.`
      },
      planBlock: {
        title: 'Plan your day',
        caption: 'Do you want to take control of your time? Use our calendar Feed!'
      },
      testimonialsBlock: {
        title: 'Users about us',
        testimonialsUsers: [
          'Developer',
          'User',
          'Designer',
          'Businessman',
          'Journalist'
        ],
        testimonialsMsg: [
          'It\'s like an ordinary notebook but easier to use',
          'Plan your vacation, create events and dreams will come true...',
          'The app allows to manage all tasks through the browser. Awesome!',
          'It\'s very easy to use, perfect app to keep all your tasks in one place!',
          'Productivity with Listick has increased so much! I can\'t imagine how I used to cope with tasks flow',
        ],
      },
      feedback: {
        text: 'Have Question?',
        link: 'Contact us'
      },
      policy: 'Privacy Policy'
    },
    // APP PAGES / COMPONENTS
    Pages: {
      Tasks: {
        title: 'Tasks',
        quickTaskPlaceholder: 'Click to quickly add a task'
      },
      Events: {
        today: 'Today',
        tomorrow: 'Tomorrow'
      },
      Project: {
        deleteDialog: {
          message: 'If you delete this project, access to all subprojects will be lost',
          discardBtn: 'Cancel',
          confirmBtn: 'Delete'
        }
      },
      DetailView: {
        DetailViewCard: {
          projectPlaceholder: 'Add project',
          timePlaceholder: 'Set time',
          description: {
            title: 'Description',
            placeholder: 'Add Description'
          },
          priority: {
            onText: 'Priority',
            offText: 'Set priority',
          },
          files: {
            dragOverMessage: 'Drag files to upload',
            uploadBtn: 'Upload file',
            uploadingMessage: 'Uploading file {fileName}',
            maxFilesCountErrorMessage: 'You can upload 5 files per task only',
            maxFileSizeErrorMessage: 'File is too big! Max file size is 10Mb',
            emptyFileErrorMessage: 'Selected file is empty'
          },
          DetailViewDeleteBtn: 'Delete'
        },
        DetailViewModals: {
          SelectProjectModal: {
            title: 'Add project',
            searchLabel: 'Find project',
            discardBtn: 'Cancel',
            confirmBtn: 'Save'
          },
          TimeModal: {
            title: 'Add time',
            TimeStart: {
              label: 'Start',
              datePlaceholder: 'Date',
              timePlaceholder: 'Time'
            },
            TimeEnd: {
              label: 'End',
              datePlaceholder: 'Date',
              timePlaceholder: 'Time'
            },
            allDaySwitch: 'All day',
            discardBtn: 'Cancel',
            confirmBtn: 'Save'
          }
        }
      },
      Navigation: {
        leftSidebar: {
          lists: [
            'Lists',
            [
              'Feed',
              'Tasks',
              'Projects'
            ]
          ]
        },
        rightSidebar: {
          search: {
            searchPlaceholder: 'Search',
            emptySearchMessage: 'No matches for your search'
          },
          fabBtn: {
            taskText: 'Task',
            eventText: 'Event',
            projectText: 'Project',
          },
          addDialog: {
            name: {
              eventPlaceholder: 'Event title..',
              taskPlaceholder: 'Task title..',
              requiredErrorMsg: 'Field is required',
            },
            start: {
              label: 'Start',
              datePlaceholder: 'Date',
              timePlaceholder: 'Time',
            },
            end: {
              label: 'End',
              datePlaceholder: 'Date',
              timePlaceholder: 'Time',
            },
            allDaySwitch: 'All day',
            discardBtn: 'Cancel',
            confirmBtn: 'Save'
          },
          addProjectDialog: {
            name: {
              placeholder: 'Project title..',
              requiredErrorMsg: 'Field is required',
            },
            discardBtn: 'Cancel',
            createBtn: 'Add',
            updateBtn: 'Save',
          }
        },
        topBar: {
          calendarTooltip: 'Calendar',
          settingsTooltip: 'Settings',
        },
        projectList: {
          addProject: 'Add project'
        }
      },
      Snackbar: {
        Login: {
          success: 'Successful auth',
          error: 'Error auth'
        },
        Logout: {
          message: 'Successful logout'
        },
        Register: {
          success: 'Registration completed',
          error: 'Registration error',
        },
        PasswordChange: {
          success: 'Password has been changed successfully',
          error: 'Failed to change password',
        },
        UserUpdate: {
          success: 'Data has been changed successfully',
          error: 'Failed to change data',
        },
        Subscribe: {
          success: 'Thank you for joining us 😊',
          error: 'Subscription Error',
          emailError: 'Invalid email'
        },
        Feedback: {
          success: 'Thank you for your message!',
          error: 'Your message was not send.',
        },
        GoogleMessages: {
          linkCalendarSuccess: 'Connected with Google Calendar',
          linkCalendarError: 'Failed to connect Google Calendar',
          unlinkCalendarSuccess: 'Google Calendar connection removed',
          unlinkCalendarError: 'Failed to remove Google Calendar connection',
        },
        Events: {
          add: 'Event added',
          addError: 'Failed to add event',
          update: 'Event updated',
          updateError: 'Failed to update event',
          done: 'Event completed',
          delete: 'Event deleted',
          deleteError: 'Failed to delete event',
          time: {
            drop: 'Time deleted',
            becomeTask: 'Time deleted, event moved to Tasks!'
          },
          project: {
            add: 'Event moved to project',
            drop: 'Event removed from project'
          },
        },
        Tasks: {
          add: 'Task added',
          addError: 'Failed to add task',
          delete: 'Task deleted',
          deleteError: 'Failed to delete task',
          update: 'Task updated',
          updateError: 'Failed to update task',
          done: 'Task completed',
          time: {
            becomeEvent: 'Time added, task moved to Feed!'
          },
          project: {
            add: 'Task moved to project',
            drop: 'Task removed from project'
          },
        },
        Project: {
          add: 'Project added',
          addError: 'Failed to add project',
          delete: 'Project deleted',
          deleteError: 'Failed to delete project',
          update: 'Project updated',
          updateError: 'Failed to update project',
        },
        Action: {
          close: 'Close',
          go: 'Go',
          goToProj: 'Go to project',
          goToEvents: 'Go to Feed',
          goToTasks: 'Go to Tasks',
        },
        Files: {
          upload: {
            success: 'File uploaded',
            commonError: 'File upload error',
            limitError: 'You reached your file size limit',
          },
          delete: {
            success: 'File was deleted',
            error: 'Failed to delete file',
          }
        }
      }
    },
    noDataText: 'No matches for your search',
    dataIterator: {
      rowsPerPageText: 'Items per page:',
      pageText: '{0}-{1} of {2}',
      reccurence: ' day of '
    },
    badge: 'badge',
    timePicker: {
      am: 'AM',
      pm: 'PM'
    }
  }
}

export default messages
