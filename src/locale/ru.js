const messages = {
  $vuetify: {
    // LOGIN PAGE
    Login: {
      LoginBtn: 'Войти',
      LoginForm: {
        email: {
          label: 'Email',
          requiredErrorMsg: 'Поле является обязательным для заполнения',
          commonErrorMsg: 'Проверьте правильность введенных данных'
        },
        password: {
          label: 'Пароль',
          requiredErrorMsg: 'Поле является обязательным для заполнения',
          minLengthErrorMsg: 'Пароль должен содержать минимум 8 символов',
          maxLengthErrorMsg: 'Пароль должен содержать максимум 40 символов',
          noSpacesErrorMsg: 'Пароль не должен содержать пробелы',
        },
        divider: 'или',
        googleBtnMessage: 'Войти через Google',
        appleBtnMessage: 'Войти с Apple',
        passwordMessage: 'Забыли пароль?',
        accountMessage: 'Нет аккаунта?',
      }
    },
    // REGISTER PAGE
    Register: {
      RegisterBtn: 'Зарегистрироваться',
      RegisterForm: {
        email: {
          label: 'Email',
          requiredErrorMsg: 'Поле является обязательным для заполнения',
          commonErrorMsg: 'Проверьте правильность введенных данных'
        },
        name: {
          label: 'Имя',
          requiredErrorMsg: 'Поле является обязательным для заполнения'
        },
        password: {
          label: 'Пароль',
          requiredErrorMsg: 'Поле является обязательным для заполнения',
          minLengthErrorMsg: 'Пароль должен содержать минимум 8 символов',
          maxLengthErrorMsg: 'Пароль должен содержать максимум 40 символов',
          noSpacesErrorMsg: 'Пароль не должен содержать пробелы',
        },
        divider: 'или',
        googleBtnMessage: 'Продолжить с Google',
        appleBtnMessage: 'Продолжить с Apple',
        accountMessage: 'Уже есть аккаунт?',
      }
    },
    // ACCOUNT PAGE
    Account: {
      header: 'Настройки',
      ProfileCard: {
        title: 'Профиль',
        form: {
          name: {
            label: 'Имя',
            requiredErrorMsg: 'Поле является обязательным для заполнения',
          },
          email: {
            label: 'Электронная почта',
            requiredErrorMsg: 'Поле является обязательным для заполнения',
            commonErrorMsg: 'Проверьте правильность введенных данных'
          },
          ProfileCardBtn: 'Сохранить'
        }
      },
      PasswordChangeCard: {
        title: 'Изменение пароля',
        form: {
          oldPassword: {
            label: 'Старый пароль',
            requiredErrorMsg: 'Поле является обязательным для заполнения',
            noSpacesErrorMsg: 'Пароль не должен содержать пробелы',
          },
          newPassword: {
            label: 'Новый пароль',
            requiredErrorMsg: 'Поле является обязательным для заполнения',
            isNotSameAsPasswordErrorMsg: 'Старый и новый пароли не должны совпадать',
            minLengthErrorMsg: 'Пароль должен содержать минимум 8 символов',
            maxLengthErrorMsg: 'Пароль должен содержать максимум 40 символов',
            noSpacesErrorMsg: 'Пароль не должен содержать пробелы',
          },
          newPasswordConfirm: {
            label: 'Новый пароль (еще раз)',
            requiredErrorMsg: 'Поле обязательно для заполнени',
            sameAsPasswordErrorMsg: 'Пароли должны совпадать',
            minLengthErrorMsg: 'Пароль должен содержать минимум 8 символов',
            maxLengthErrorMsg: 'Пароль должен содержать максимум 40 символов',
            noSpacesErrorMsg: 'Пароль не должен содержать пробелы',
          },
          PasswordChangeCardBtn: 'Изменить'
        },
      },
      GoogleCalendarLinkCard: {
        title: 'Google Calendar',
        caption: 'Подключите двустороннюю синхронизацию Google Календаря и событий, запланированных в Ленте',
        GoogleCalendarLinkCardBtn: 'Подключить Google аккаунт',
        syncStatus: {
          message: 'Настроена синхронизация с календарем {gcalName}. Все события из календаря {gcalName} записываются в Ленту, а все события из Ленты записываются в календарь {gcalName}. ',
          syncStatusBtn: 'Отменить синхронизацию'
        },
        calendarListDialog: {
          title: 'Подключение к Google Календарю',
          selectCalendarText: 'Выбрать календарь',
          discardBtn: 'Отмена',
          confirmBtn: 'Подключить'
        }
      },
      LanguageSettingsCard: {
        title: 'Язык',
        selectLanguageLabel: 'Язык',
        LanguageSettingsCardBtn: 'Сохранить',
        languages: [
          'Русский',
          'Английский'
        ]
      },
      DeleteAccountCard: {
        title: 'Удаление аккаунта',
        userWarningText: 'Обратите внимание на то, что при удалении аккаунта Tasker доступ ко всем задачам и событиям в аккаунте будет потерян.',
        DeleteAccountCardBtn: 'Удалить аккаунт',
        deleteAccountDialog: {
          title: 'Удалить аккаунт?',
          deleteWarningText: 'Это действие невозможно отменить, вы уверены?',
          discardBtn: 'Отмена',
          confirmBtn: 'OK'
        }
      },
      StorageCard: {
        title: 'Хранилище',
        StorageCardBtn: 'Увеличить',
        storageUsage: 'Использовано {usedStorageSize} из {fullStorageSize}'
      },
      LogoutCard: {
        title: 'Выход',
        caption: 'Выход из аккаунта',
        LogoutCardBtn: 'Выход'
      },
      feedback: {
        text: 'Если у вас возникли вопросы, перейдите по данной',
        link: 'ссылке'
      },
    },
    Feedback: {
      title: 'Свяжитесь с нами',
      name: {
        label: 'Имя'
      },
      email: {
        label: 'Email',
        commonErrorMsg: 'Проверьте правильность введенных данных'
      },
      message: {
        label: 'Сообщение'
      },
      FeedbackBtn: 'Сохранить'
    },
    Home: {
      title: 'Планируйте по-новому с Listick',
      caption: `Оставьте все ваши идеи, встречи и задачи в Listick. Мы напомним о них.
      Скачайте наше приложение!`,
      navbar: {
        login: 'Вход',
        register: 'Регистрация'
      },
      subscribeForm: {
        name: {
          placeholder: 'Email'
        },
        btn: 'Присоединиться'
      },
      startBlock: {
        title: 'Начните с простого',
        caption: `Listick можно использовать так, как удобно вам. 
        Используйте заметки, если что-то пришло на ум.`
      },
      planBlock: {
        title: 'Распланируйте свой день',
        caption: 'Хотите контролировать время? Используйте нашу календарную ленту!'
      },
      testimonialsBlock: {
        title: 'О нас говорят',
        testimonialsUsers: [
          'Разработчик',
          'Пользователь',
          'Дизайнер',
          'Предприниматель',
          'Журналист'
        ],
        testimonialsMsg: [
          'Обычный ежедневник, только удобнее и легче',
          'Планируешь путешествие, создаешь события и мечты начинают оживать…',
          'Приложение позволяет смотреть список дел прям в браузере. Восторг!',
          'Удобно и понятно, отлично получается держать все задачи в одном месте!',
          'Продуктивность с Listick выросла в разы! Не представляю, как раньше справлялась со всем потоком задач',
        ],
      },
      feedback: {
        text: 'Остались вопросы?',
        link: 'Свяжитесь с нами!'
      },
      policy: 'Политика конфиденциальности'
    },
    // APP PAGES / COMPONENTS
    Pages: {
      Tasks: {
        title: 'Задачи',
        quickTaskPlaceholder: 'Быстрое добавление задачи'
      },
      Events: {
        today: 'Сегодня',
        tomorrow: 'Завтра'
      },
      Project: {
        deleteDialog: {
          message: 'Удаление этого проекта приведет к удалению всех связанных с ним проектов',
          discardBtn: 'Отмена',
          confirmBtn: 'Удалить'
        }
      },
      DetailView: {
        DetailViewCard: {
          projectPlaceholder: 'Добавить в проект',
          timePlaceholder: 'Назначить время',
          description: {
            title: 'Описание',
            placeholder: 'Добавить опиcание'
          },
          priority: {
            onText: 'Приоритетная задача',
            offText: 'Установить приоритет',
          },
          files: {
            dragOverMessage: 'Перетащите файлы, чтобы загрузить их',
            uploadBtn: 'Загрузить файл',
            uploadingMessage: 'Загружаем файл {fileName}',
            maxFilesCountErrorMessage: 'К одной задаче можно прикрепить не более 5 файлов!',
            maxFileSizeErrorMessage: 'Файл слишком большой! Максимальный размер 10 Мб',
            emptyFileErrorMessage: 'Выбранный файл пуст, загрузите другой файл'
          },
          DetailViewDeleteBtn: 'Удалить'
        },
        DetailViewModals: {
          SelectProjectModal: {
            title: 'Выбрать проект',
            searchLabel: 'Найти проект',
            discardBtn: 'Отмена',
            confirmBtn: 'Сохранить'
          },
          TimeModal: {
            title: 'Назначить время',
            TimeStart: {
              label: 'Начало',
              datePlaceholder: 'Дата',
              timePlaceholder: 'Время'
            },
            TimeEnd: {
              label: 'Окончание',
              datePlaceholder: 'Дата',
              timePlaceholder: 'Время'
            },
            allDaySwitch: 'Весь день',
            discardBtn: 'Отмена',
            confirmBtn: 'Сохранить'
          }
        }
      },
      Navigation: {
        leftSidebar: {
          lists: [
            'Списки',
            [
              'Лента',
              'Задачи',
              'Проекты'
            ]
          ]
        },
        rightSidebar: {
          search: {
            searchPlaceholder: 'Найти',
            emptySearchMessage: 'Ничего не найдено'
          },
          fabBtn: {
            taskText: 'Задача',
            eventText: 'Событие',
            projectText: 'Проект',
          },
          addDialog: {
            name: {
              eventPlaceholder: 'Название события..',
              taskPlaceholder: 'Название задачи..',
              requiredErrorMsg: 'Поле является обязательным для заполнения',
            },
            start: {
              label: 'Начало',
              datePlaceholder: 'Дата',
              timePlaceholder: 'Время',
            },
            end: {
              label: 'Окончание',
              datePlaceholder: 'Дата',
              timePlaceholder: 'Время',
            },
            allDaySwitch: 'Весь день',
            discardBtn: 'Отмена',
            confirmBtn: 'Сохранить'
          },
          addProjectDialog: {
            name: {
              placeholder: 'Название проекта..',
              requiredErrorMsg: 'Поле является обязательным для заполнения',
            },
            discardBtn: 'Отмена',
            createBtn: 'Создать',
            updateBtn: 'Сохранить',
          }
        },
        topBar: {
          calendarTooltip: 'Показать календарь',
          settingsTooltip: 'Настройки',
        },
        projectList: {
          addProject: 'Добавить проект'
        }
      },
      Snackbar: {
        Login: {
          success: 'Авторизация прошла успешно',
          error: 'Ошибка авторизации'
        },
        Logout: {
          message: 'Вы вышли из приложения'
        },
        Register: {
          success: 'Регистрация прошла успешно',
          error: 'Ошибка регистрации',
        },
        PasswordChange: {
          success: 'Пароль был изменен',
          error: 'Ошибка изменения пароля',
        },
        UserUpdate: {
          success: 'Данные изменены успешно',
          error: 'Ошибка изменения данных',
        },
        Subscribe: {
          success: 'Спасибо за подписку😊',
          error: 'Не удалось подписаться',
          emailError: 'Некорректный email'
        },
        Feedback: {
          success: 'Спасибо за Ваш отзыв!',
          error: 'Не удалось отправить сообщение',
        },
        GoogleMessages: {
          linkCalendarSuccess: 'Синхронизация с Google Календарем установлена',
          linkCalendarError: 'Ошибка подключения Google Календаря',
          unlinkCalendarSuccess: 'Синхронизация с Google Календарем отменена',
          unlinkCalendarError: 'Ошибка отмены синхронизации Google Календаря',
        },
        Events: {
          add: 'Событие создано',
          addError: 'Ошибка создания события',
          update: 'Событие обновлено',
          updateError: 'Ошибка обновления события',
          done: 'Событие выполнено',
          delete: 'Событие удалено',
          deleteError: 'Ошибка удаления события',
          time: {
            drop: 'Время удалено',
            becomeTask: 'Время удалено, ваше событие теперь в задачах!'
          },
          project: {
            add: 'Событие добавлено в проект',
            drop: 'Событие убрано из проекта'
          },
        },
        Tasks: {
          add: 'Задача создана',
          addError: 'Ошибка создания задачи',
          delete: 'Задача удалена',
          deleteError: 'Ошибка удаления задачи',
          update: 'Задача обновлена',
          updateError: 'Ошибка обновления задачи',
          done: 'Задача выполнена',
          time: {
            becomeEvent: 'Время установлено, ваша задача теперь на ленте!'
          },
          project: {
            add: 'Задача добавлена в проект',
            drop: 'Задача убрана из проекта'
          },
        },
        Project: {
          add: 'Проект создан',
          addError: 'Ошибка создания проекта',
          delete: 'Проект удален',
          deleteError: 'Ошибка удаления проекта',
          update: 'Проект изменен',
          updateError: 'Ошибка изменения проекта',
        },
        Action: {
          close: 'Закрыть',
          go: 'Перейти',
          goToProj: 'Перейти к проекту',
          goToEvents: 'Перейти к Ленте',
          goToTasks: 'Перейти к списку задач',
        },
        Files: {
          upload: {
            success: 'Файл успешно загружен',
            commonError: 'Ошибка загрузки файла',
            limitError: 'Хранилище файлов заполнено',
          },
          delete: {
            success: 'Файл успешно удален',
            error: 'Ошибка удаления файла',
          }
        }
      }
    },
    noDataText: 'Нет данных',
    dataIterator: {
      rowsPerPageText: 'Элементов на страницу:',
      pageText: '{0}-{1} из {2}',
      reccurence: '-й день из '
    },
    badge: 'знак',
    timePicker: {
      am: 'AM',
      pm: 'PM'
    }
  }
}

export default messages
