import Vue from 'vue'
import VueI18n from 'vue-i18n'
import store from '../store'
import moment from 'moment'
import ru from '@/locale/ru'
import en from '@/locale/en'

Vue.use(VueI18n)

moment.locale(store.getters.locale)

const i18n = new VueI18n({
  locale: store.getters.locale,
  messages: {
    en: en,
    ru: ru
  }
})

export default i18n